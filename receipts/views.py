from django.shortcuts import render, redirect
from receipts.models import ExpenseCategory, Account, Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseForm, AccountForm


# Create your views here.
@login_required
def show_receipt_list(request):
    item = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt_list": item}
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipts = form.save(commit=False)
            receipts.purchaser = request.user
            receipts.save()
            return redirect("home")
    else:
        form = ReceiptForm()
        context = {
            "form": form,
        }
        return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    item = ExpenseCategory.objects.filter(owner=request.user)
    context = {"expense_list": item}
    return render(request, "receipts/expense.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseForm()
        context = {
            "form": form,
        }
        return render(request, "receipts/create_category.html", context)


@login_required
def account_list(request):
    item = Account.objects.filter(owner=request.user)
    context = {"account_list": item}
    return render(request, "receipts/account.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
        context = {
            "form": form,
        }
        return render(request, "receipts/create_account.html", context)
